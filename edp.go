/*
 * @Author: owen 
 * @Date: 2017-12-15 14:22:50 
 * @Last Modified by:   owen 
 * @Last Modified time: 2017-12-15 14:22:50 
 */
 package oneNetGo


import (
	"fmt"
	"bytes"
	"encoding/json"
	"net/http"
)

const verifyMsg string = "msg"
const verifyNoce string = "nonce"
const verifysignature string = "signature"

const TypeFail int =-1
const TypeVerify int = 0
const TypeMsg int = 1

// 数据点消息(type=1)
// 示例：
// 		{
// 			 "msg": {
// 						"type": 1,
// 						"dev_id": 2016617,
// 						 "ds_id": "datastream_id",
// 						 "at": 1466133706841,
// 						 "value": 42
// 			  },
// 			"msg_signature": "message signature",
// 			"nonce": "abcdefgh"
// 		}
type MsgUtil struct {
	Type int `json:"type"`
	DevID int `json:"dev_id"`
	DsID string `json:"ds_id"`
	At int	`json:"at"`
	Value string `json:"value"`
}
type OneNetMsg struct{
	Msg MsgUtil `json:"msg"`
	MsgSignature string `json:"msg_signature"`
	Nonce string `json:"nonce"`
}


func GetEdpTransferMsg(w http.ResponseWriter,r *http.Request)(OneNetMsg,int){
	oneNetMsg := OneNetMsg{}
	// URL及token验证,oneNet中是以get的形式来获取msg的验证
	if r.Method==http.MethodGet {
		msg := r.FormValue(verifyMsg)
		if msg != ""{
			w.Write([]byte(msg))
			return oneNetMsg,TypeVerify
		}
	}else{
		buf:=new(bytes.Buffer)
		buf.ReadFrom(r.Body)
		fmt.Println("edp get msg:",buf.String())
		err:= json.Unmarshal(buf.Bytes(),&oneNetMsg)
		fmt.Println("edp get msg22:",buf.String(),err)
		if err != nil{
			return oneNetMsg,TypeFail
		}else{
			return oneNetMsg,TypeMsg
		}
	}
	return oneNetMsg,TypeFail
}