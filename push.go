/*
 * @Author: owen 
 * @Date: 2017-11-27 16:06:14 
 * @Last Modified by: owen
 * @Last Modified time: 2017-11-27 16:51:39
 * 
 * 用于oneNet平台上，把私人服务器上的数据（或cmd）推送给接入到onNet的设备
 * 根据https://open.iot.10086.cn/doc/art257.html#68  oneNet上的指南
 */

package oneNetGo

import (
	"bytes"
	"io/ioutil"
	"fmt"
	"strconv"
	"net/http"


)

const this_file string = "push.go"
const cmdUrl string = "http://api.heclouds.com/cmds"
/**
 * edp 协议上的，url参数结构体
 */
type UrlEdpParam struct {
	Device_id int		//接收该数据的设备ID（必选）。
	Qos	int			//是否需要响应，默认为0。本参数仅当type=0时有效；
						//0：不需要响应，即最多发送一次，不关心设备是否响应；
						//1：需要响应，如果设备收到命令后没有响应，则会在下一次设备登陆时若命令在有效期内(有效期定义参见timeout参数）则会继续发送。
	Timeout	int			//命令有效时间，默认0。	
						//0：在线命令，若设备在线,下发给设备，若设备离线，直接丢弃；	
						//>0： 离线命令，若设备在线，下发给设备，若设备离线，在当前时间加timeout时间内为有效期，有效期内，若设备上线，则下发给设备。单位：秒，有效围：0~2678400。
	Cmd_type int		//默认0。0：发送CMD_REQ包，1：发送PUSH_DATA包
}
/**
 * htpp handers （http头部）参数
 */
type HttpHanders struct {
	ApiKey string 		//masterkey或者设备apikey
	ContentType string  
}
/**
 * 推送数据或cmd到设备,dep协议
 * urlparam:@url的连接参数
 * handers @http的头参数设置
 * data：@json字符串，"{\"cmd\":\"hello ,I'm the push msg\"}"
 */
func PushData(urlparam UrlEdpParam,handers HttpHanders,data string){
	client:= &http.Client{}	
	url := cmdUrl+"?"+"device_id="+strconv.Itoa(urlparam.Device_id)+"&"+
	"qos="+strconv.Itoa(urlparam.Qos)+"&"+"timeout="+strconv.Itoa(urlparam.Timeout)+"&"+
	"type="+strconv.Itoa(urlparam.Cmd_type)
	fmt.Println(this_file,url)
	reader := bytes.NewReader([]byte(data))
	req,err := http.NewRequest("POST", url,reader)
	if err != nil {
		fmt.Println(this_file,"client request err:",err)
		return
	}
	req.Header.Set("api-key", handers.ApiKey)
	req.Header.Set("Content-Type", handers.ContentType)


	resp,err:=client.Do(req)
	defer resp.Body.Close()
	body,err:=ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(this_file,"client do  err")
	}
	fmt.Println(this_file,string(body))
}