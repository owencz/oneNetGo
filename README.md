# oneNetGo
go语言，oneNet转发第三方服务器解析单元

==备注实现：
1：目前只实现了单包的解码
2：目前实现了oneNet发送url tokan 并返回msg验证实现绑定

==用法如下
go get gitee.com/owencz/oneNetGo

例程如下
要把 func transfer（w，r）默认是resetful api的Handlefunc功能来使用
/**
 * oneNet数据转发
 */
func transfer(w http.ResponseWriter,r *http.Request){
	msg,ret:=oneNetGo.GetTransferMsg(w,r)
	if ret == oneNetGo.TypeFail {
		olog.Debug("recevice error request")
	}else if ret == oneNetGo.TypeVerify{
		olog.Debug("oneNet 接入 tokan 成功")
	}else{
		olog.Debug("成功获取数据：", msg)
	}

}

其中的msg是收到oneNet发包后解析出来的数据结构体
oneNetGo.TypeFail       定义解析失败
oneNetGo.TypeVerify     定义解析到的是tokan检验
oneNetGo.TypeMsg        定义解析到是数据包